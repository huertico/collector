==============
huertico-water
==============

  .. image:: https://assets.gitlab-static.net/uploads/-/system/project/avatar/11030464/water.png?width=64
     :target: https://gitlab.com/huertico/huertico-water
     :alt: water sources management

Open Hardware/Software for automated plant growing.

Irrigation unit, responsible for watering plants in a specific watering zone when the server unit directed.

License
=======

GPL 3.  See the `LICENSE <https://gitlab.com/huertico/huertico-water/blob/master/LICENSE>`_ file for more details.

Authors
=======

`huertico <https://gitlab.com/huertico>`_ is developed by `Manuel Delgado López <https://twitter.com/valarauco>`_, `Víctor Castro Mattei <https://geekl0g.wordpress.com/author/constrict0r>`_ and `Alejandro Rodríguez <http://arguez.com>`_.
