#define MQTT_DEBUG

//Topics
const String action_topic = "huertico/"+String(ESP.getChipId(), HEX)+"/act";
const String register_topic = "huertico/register";

//Subscribes
Adafruit_MQTT_Subscribe action_sub = Adafruit_MQTT_Subscribe(mqtt, action_topic.c_str());


/*__________________________________________________________MQTT_FUNCTIONS__________________________________________________________*/
void RegisterToMQTT() {
  MQTTConnect();

  Adafruit_MQTT_Publish register_pub = Adafruit_MQTT_Publish(mqtt, register_topic.c_str());
  //                       doc has 2 elemets
  const size_t capacity = JSON_OBJECT_SIZE(2);
  DynamicJsonDocument doc(capacity);
  doc["id"] = String(ESP.getChipId(), HEX);
  doc["type"] = HUERTICO_TYPE;

  String serialized_data = "";
  serializeJson(doc, serialized_data);
  serial_println("Publishing register JSON data");
  if (!register_pub.publish(serialized_data.c_str())) {
    serial_println("Error publishing register JSON data");
  }
  if (! mqtt->ping()) {
    mqtt->disconnect();
  }
}

void SubscribeMQTT() {
  mqtt->subscribe(&action_sub); 
}

void ProcessMQTT() {
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).
  MQTTConnect();
  
  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt->readSubscription(5000))) {
    if (subscription == &action_sub) {
      String mqtt_message = (char *)action_sub.lastread;
      serial_print("DATA SUB: ");
      serial_println(mqtt_message);
      DynamicJsonDocument jsonDoc(1024);
      deserializeJson(jsonDoc, mqtt_message);
      serial_print(jsonDoc);
      serial_println("");
      if (!jsonDoc.isNull()) {
        JsonObject json = jsonDoc.as<JsonObject>();
        String mqtt_action = json["act"].as<String>();
        if (mqtt_action == "open") {
          digitalWrite(relay00_vcc, LOW);
        } else {
          digitalWrite(relay00_vcc, HIGH);
        }
      } 
    }
  }
  
  // ping the server to keep the mqtt connection alive
  // NOT required if you are publishing once every KEEPALIVE seconds
  if (! mqtt->ping()) {
    mqtt->disconnect();
  }
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
bool MQTTConnect() {
  // Stop if no MQTT
  if (! mqtt) {
    return false;
  }
  // Stop if already connected.
  if (mqtt->connected()) {
    return true;
  }

  serial_print("Connecting to MQTT... ");

  int8_t ret;
  uint8_t retries = 3;
  while ((ret = mqtt->connect()) != 0 && retries > 0) { // connect will return 0 for connected
    serial_println(mqtt->connectErrorString(ret));
    serial_println("Retrying MQTT connection in 5 seconds...");
    mqtt->disconnect();
    delay(5000);  // wait 5 seconds
    retries--;
  }

  if (mqtt->connected()) {
    serial_println("MQTT Connected!");
    return true;
  }
  serial_println("MQTT connection failed!");
  return false;
}
