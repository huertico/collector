#define HUERTICO_DEBUG false
#define HUERTICO_SERIAL_DEBUG true
#define HUERTICO_TYPE "water"
#include <ESP8266WiFi.h>    //https://github.com/esp8266/Arduino
#include <WiFiManager.h>   //https://github.com/tzapu/WiFiManager
#include <FS.h>            //https://github.com/esp8266/arduino-esp8266fs-plugin // https://www.instructables.com/id/Using-ESP8266-SPIFFS/
#include <ArduinoJson.h>   //https://github.com/bblanchon/ArduinoJson/
#include "Adafruit_MQTT.h" //https://github.com/adafruit/Adafruit_MQTT_Library/
#include "Adafruit_MQTT_Client.h"

// Relay Vcc GPIO //
#define relay00_vcc 4 //Wemos D2
#define relay01_vcc 0    //Wemos D3
#define debug_push_btn 14 //Wemos D5
// TODO: who to know a relay is connected?
// TODO: better way to setup lots of relays (I2C, shift registers, mux)

// Huertico default configs //
String mqtt_server = "huerticomqtt.local";
int mqtt_port = 1883;
bool mqtt_failed = false;
bool shouldSaveConfig = false;

// Objects init //
File fs_upload_file;
WiFiClient mqtt_wclient;  // Create an ESP8266 WiFiClient class to connect to the MQTT server.
//WiFiClientSecure mqtt_wclient; // or... use WiFiFlientSecure for SSL
Adafruit_MQTT_Client* mqtt;

/*__________________________________________________________MAIN_FUNCTIONS__________________________________________________________*/

void setup() {
  Serial.begin(115200);        // Start the Serial communication to send messages to the computer
  delay(10);
  Serial.println("\r\n");

  //setup pinmode
  pinMode(relay00_vcc, OUTPUT);
  pinMode(relay01_vcc, OUTPUT);
  digitalWrite(relay00_vcc, HIGH);
  digitalWrite(relay01_vcc, HIGH);
  if (HUERTICO_DEBUG) {
    pinMode(debug_push_btn, INPUT);
    pinMode(16, OUTPUT);

  }
  
  if ( ! HUERTICO_DEBUG ) {
    startSPIFFS();
    startWiFi();
    startMQTT();
    // if no MQTT start Server
    if (! mqtt || ! mqtt->connected()) {
      set_error("Can not connect to MQTT");
      ESP.reset();
    }
  }
}

void loop() {
  if ( HUERTICO_DEBUG ) {
    /*
     * Use this section to test sensors and data
     * without mqtt an wifi 
     */
    int pushed =  digitalRead(debug_push_btn);   // read push btn
    if (pushed == LOW){
      serial_println("Pushed");
      digitalWrite(relay00_vcc, LOW);
      digitalWrite(relay01_vcc, LOW);
    }else{
      serial_println("NOT Pushed");
      digitalWrite(relay00_vcc, HIGH);
      digitalWrite(relay01_vcc, HIGH);
    }
    
  } else {
    ProcessMQTT();
    delay(1000);
  }
}
