// Huertico Name
const String huertico_name = "Huertico_" + String(ESP.getChipId(), HEX); // Huertico Sensor Name

/*__________________________________________________________SETUP_FUNCTIONS__________________________________________________________*/

void startSPIFFS() { // Start the SPIFFS and list all contents
  SPIFFS.begin();                             // Start the SPI Flash File System (SPIFFS)
  serial_println("SPIFFS started. Contents:");
  if (HUERTICO_SERIAL_DEBUG){
    Dir dir = SPIFFS.openDir("/");
    while (dir.next()) {                      // List the file system contents
      String fileName = dir.fileName();
      size_t fileSize = dir.fileSize();
      serial_print("\tFS File: "+fileName+", size: "+formatBytes(fileSize)+"\r\n");
    }
    serial_print("\n");
  }
}

void startWiFi() {
  serial_println("Starting WiFi");
  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  //reset saved settings
  //wifiManager.resetSettings();
  //disable debug on serial console
  if (!HUERTICO_SERIAL_DEBUG)
    wifiManager.setDebugOutput(false);

  loadConfigs();
  String error_message = get_error();

  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  // Set config parameters for Huertico Satallite
  WiFiManagerParameter custom_header("<p><strong>Huertico Wind Settings</strong></p>");
  WiFiManagerParameter custom_lbl_mqtt_server("<label for=\"server\">Server hostname or address</label>");
  WiFiManagerParameter custom_mqtt_server("server", "Server address", mqtt_server.c_str(), 40);
  WiFiManagerParameter custom_lbl_mqtt_port("<br /><br /><label for=\"port\">Server port (default: 1883)</label>");
  WiFiManagerParameter custom_mqtt_port("port", "Server port", String(mqtt_port).c_str(), 6);
  wifiManager.addParameter(&custom_header);
  if (error_message.length() > 1) {
    error_message = "<div style=\"padding: 20px;background-color: #ff9800;color: white;opacity: 0.83;transition: opacity 0.6s;margin-bottom: 15px;\">"+ error_message +"</div>";
    const char * error_html = error_message.c_str();
    WiFiManagerParameter custom_error_msg(error_html);
    wifiManager.addParameter(&custom_error_msg);
  }
  wifiManager.addParameter(&custom_lbl_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_lbl_mqtt_port);
  wifiManager.addParameter(&custom_mqtt_port);

  //fetches ssid and pass from eeprom and tries to connect
  //if it does not connect it starts an access point with the specified huertico_name
  //and goes into a blocking loop awaiting configuration
  if (error_message.length() > 1) {
    wifiManager.startConfigPortal(huertico_name.c_str());
  } else {
    wifiManager.autoConnect(huertico_name.c_str());
  }
  //or use this for auto generated name ESP + ChipID
  //wifiManager.autoConnect();

  //read updated parameters
  mqtt_server = String(custom_mqtt_server.getValue());
  mqtt_port = String(custom_mqtt_port.getValue()).toInt();

  if (shouldSaveConfig) {
    saveConfigs();
  }
}

void startMQTT() {
  if (loadConfigs()) {
    mqtt = new Adafruit_MQTT_Client(&mqtt_wclient, mqtt_server.c_str(), mqtt_port);
    //mqtt = new Adafruit_MQTT_Client(&mqtt_wclient, toChar(mqtt_server), mqtt_port); //Dunno why this isn't working
    SubscribeMQTT();
    if (MQTTConnect()) {
      if (! mqtt->ping()) {
        serial_println("No ping from MQTT, disconnect!");
        mqtt->disconnect();
      }
    }
  }
}
