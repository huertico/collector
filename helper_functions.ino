/*__________________________________________________________HELPER_FUNCTIONS__________________________________________________________*/
const String error_filepath = "/last_error";
const String config_path = "/.huertico.conf";

String formatBytes(size_t bytes) { // convert sizes in bytes to KB and MB
  if (bytes < 1024) {
    return String(bytes) + "B";
  } else if (bytes < (1024 * 1024)) {
    return String(bytes / 1024.0) + "KB";
  } else if (bytes < (1024 * 1024 * 1024)) {
    return String(bytes / 1024.0 / 1024.0) + "MB";
  }
}

void saveConfigCallback () {
  serial_println("Should save config");
  shouldSaveConfig = true;
  set_error("");
}

bool loadConfigs() {
  bool result = true;
  if (SPIFFS.exists(config_path)) {
    //file exists, reading and loading
    serial_println("reading config file");
    File conf_file = SPIFFS.open(config_path, "r");
    if (conf_file) {
      serial_println("opened config file");
      size_t fsize = conf_file.size();
      // Allocate a buffer to store contents of the file.
      std::unique_ptr<char[]> buf(new char[fsize]);

      conf_file.readBytes(buf.get(), fsize);
      DynamicJsonDocument jsonDoc(1024);
      deserializeJson(jsonDoc, buf.get());
      serial_print(jsonDoc);
      serial_println("");
      if (!jsonDoc.isNull()) {
        JsonObject json = jsonDoc.as<JsonObject>();
        serial_println("\nparsed json");
        mqtt_server = json["mqtt_server"].as<String>();
        if (json["mqtt_port"]) {
          mqtt_port = json["mqtt_port"];
        }
      } else {
        serial_println("failed to load json config");
        result = false;
      }
      conf_file.close();
    }
  } else {
    serial_println("No json config");
    result = false;
  }

  return result;
}

bool saveConfigs() {
  //                       doc has 2 elemets
  const size_t capacity = JSON_OBJECT_SIZE(2);
  //TODO: Fix check config data (sanitaze)
  DynamicJsonDocument json(capacity);
  json["mqtt_server"] = mqtt_server;
  json["mqtt_port"] = mqtt_port;

  File conf_file = SPIFFS.open(config_path, "w");
  if (!conf_file) {
    serial_println("failed to open config file for writing");
    return false;
  }
  serial_println("Saved Json: ");
  serial_print(json);
  serial_println("");
  serializeJson(json, conf_file);
  conf_file.close();
  return true;
}

void set_error (String error_text) {
  serial_println("Save Error Message: "+error_text);
  File last_error_file = SPIFFS.open(error_filepath, "w");
  if(last_error_file){
    last_error_file.write((uint8_t *)error_text.c_str(), error_text.length()+1);
    last_error_file.close();
  }
}

String get_error () {
  String error_message;
  if (SPIFFS.exists(error_filepath)) {
    File last_error_file = SPIFFS.open(error_filepath, "r");
    if(last_error_file){
      size_t fsize = last_error_file.size();
      // Allocate a buffer to store contents of the file.
      std::unique_ptr<char[]> buf(new char[fsize]);

      last_error_file.readBytes(buf.get(), fsize);
      error_message = buf.get();
      last_error_file.close();
    }
  }
  serial_println("Get Error Message: "+error_message);
  return error_message;
}

void serial_println (String message){
  if (HUERTICO_SERIAL_DEBUG)
    Serial.println(message);
}

void serial_print (String message){
  if (HUERTICO_SERIAL_DEBUG)
    Serial.print(message);
}

void serial_print (DynamicJsonDocument json){
  if (HUERTICO_SERIAL_DEBUG)
    serializeJson(json, Serial);
}
